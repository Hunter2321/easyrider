package com.easy.easyriderapp.models


data class Conversation(
    var receiverName : String? = null,
    var isRead : Boolean = false,

    var receiverId:String? = null,
    var lastMessage : String? = null


)