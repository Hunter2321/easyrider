package com.easy.easyriderapp.models

import android.location.Location

class ExtendedLocation(val location: Location, val address: String)