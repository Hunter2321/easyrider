package com.easy.easyriderapp.services

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import com.easy.easyriderapp.R

class ProgressDialogService{


    var progressDialog: ProgressDialog? = null
    fun create(context: Context): ProgressDialog? {
        progressDialog = ProgressDialog(context, R.style.MyProgressBarUploadingStyle)
        progressDialog?.setCancelable(false)
        return progressDialog
    }

    fun setMessage(message: String): ProgressDialog? {
        progressDialog?.let {
            progressDialog!!.setMessage(message)
        }
        return progressDialog
    }

    fun setCancelButton(setCancelAction: (Unit) -> Unit): ProgressDialog? {
        progressDialog?.let {
            progressDialog?.setButton(
                DialogInterface.BUTTON_NEGATIVE,
                "Cancel",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        setCancelAction(Unit)
                        progressDialog!!.hide()
                    }
                })
        }
        return progressDialog
    }

    fun showDialog() {
        progressDialog?.let {
            progressDialog!!.show()
        }
    }

    fun hideDialog(){
        progressDialog?.let {
            progressDialog!!.hide()
            progressDialog =null
        }
    }
}





