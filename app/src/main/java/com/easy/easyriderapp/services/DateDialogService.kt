package com.easy.easyriderapp.services

import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import java.util.*

class DateDialogService (val activity: Activity,val listener: DatePickerDialog.OnDateSetListener,val cancelListener: DialogInterface.OnCancelListener? ,var minDate : Long = Date().time-100, var maxDate :Long? =null) {



    fun showDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)



        val dialog = DatePickerDialog(activity, listener, year, month, day)
        dialog.datePicker.minDate = minDate
        maxDate?.let{
            dialog.datePicker.maxDate = maxDate as Long
        }
        dialog.setOnCancelListener(cancelListener)

        dialog.show()
    }



}