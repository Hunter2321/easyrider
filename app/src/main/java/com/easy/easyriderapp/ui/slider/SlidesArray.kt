package com.easy.easyriderapp.ui.slider

import com.easy.easyriderapp.models.Slide

val SLIDE_ID = "SLIDE_ID"
val slidesArray = listOf<Slide>(
    Slide("Welcome to EasyRider - the first social app for cyclist", "main_logo", "Find buddies for every trip. \nWhere you want and when you want. \nAll around the world!", 0),
    Slide("Make a trip", "map_logo", "Have you heard of a cool spot? \n Get your buddies or find new ones. \nThis is so easy!",1),
    Slide("Accept the invitation", "invitation_logo", "Get notifications \nabout trips in your area. \nJust join!",2),
    Slide("Find a trip", "search_logo", "Join the trip. \n Wherever you want, whenever you want. Around the world!",3)
)



