package com.easy.easyriderapp.ui.main.events.map

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer

import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.events.EventsMapFragmentViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.easy.easyriderapp.R
import com.easy.easyriderapp.services.ProgressDialogService
import com.easy.easyriderapp.utils.PermissionsHelper
import com.easy.easyriderapp.utils.VectorToBitmapConverter
import com.easy.easyriderapp.utils.gpsHelper
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*


class EventsMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.InfoWindowAdapter {


    var mapView: MapView? = null
    var googleMap: GoogleMap? = null
    private val dataLoadingProgressDialog = ProgressDialogService()

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: EventsMapFragmentViewModel

    private lateinit var mapHelper: MapInfoWindowHelper

    private lateinit var infoWindow: ViewGroup
    private lateinit var buttonInfo: View
    private lateinit var mapWrapperLayout: MapWrapperLayout


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    val navigateToEventViewFromWindowInfo = { event: Event ->
        val args = Bundle()
        args.putString("EVENT_NAME", event.eventName)
        args.putString("DESCRIPTION", event.description)
        args.putString("CATEGORY", event.category)
        args.putDouble("LAT", event.loc_lat as Double)
        args.putDouble("LNG", event.loc_lng as Double)
        args.putString("DATE_STRING", "${event.day}/${event.month}/${event.year}")
        args.putString("ADMIN_NAME", event.creatorName)

        findNavController().navigate(
            R.id.eventView,
            args,
            NavOptions.Builder().setPopUpTo(R.id.eventsMap, false).build()
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.events_map_fragment, container, false)
        dataLoadingProgressDialog.run {
            create(context as Context)
            setMessage("Data loading...")
            showDialog()
        }
        infoWindow = layoutInflater.inflate(R.layout.event_map_info, null) as ViewGroup
        buttonInfo = infoWindow
        mapWrapperLayout = v.findViewById(R.id.map_linear_layout)

        mapHelper =
            MapInfoWindowHelper(context as Context, buttonInfo, mapWrapperLayout, navigateToEventViewFromWindowInfo)

        mapView = v?.findViewById(R.id.allEventsMapView)
        mapView?.onCreate(savedInstanceState)
        mapView?.onResume()
        mapView?.getMapAsync(this)
        return v
    }

    override fun onStart() {
        super.onStart()
        viewModel.getListOfEvents.observe(this, Observer {
            dataLoadingProgressDialog.hideDialog()
            var isEventGroupEmpty = true

            for (event in it) {
                isEventGroupEmpty = false
                setEventOnMap(event, googleMap)
            }
            if (isEventGroupEmpty) {
                Toast.makeText(context, "There are not any events, try change search settings...", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EventsMapFragmentViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.events_menu, menu)
    }

    override fun onMapReady(p0: GoogleMap?) {
        activity ?: return
        p0 ?: return
        if (!isAdded) return

        googleMap = p0

        mapHelper.initMapWrapperLayout(googleMap as GoogleMap)
        mapHelper.setInfoButtonListener()

        googleMap?.setInfoWindowAdapter(this)

        mapHelper.setOnMarkerClickListener(googleMap as GoogleMap)

        viewModel.getUserSearchInfo.observe(this, Observer {
            if (it.range < 400 && it.loc_lng != null && it.loc_lat != null) {

                googleMap?.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            it.loc_lat as Double,
                            it.loc_lng as Double
                        ), 14.0f
                    )
                )

                p0.addCircle(CircleOptions().run {
                    center(LatLng(it.loc_lat as Double, it.loc_lng as Double))
                    radius((it.range * 1000).toDouble())
                        .strokeColor(Color.BLACK)
                })
            }
        })

    }

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return mapHelper.getInfoWindow(p0 as Marker, infoWindow)
    }

    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapView?.onLowMemory()
        super.onLowMemory()
    }

    override fun onPause() {
        mapView?.onPause()

        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()

    }

    @SuppressLint("MissingPermission")
    fun setEventOnMap(event: Event, googleMap: GoogleMap?) {
        googleMap ?: return
        activity ?: return
        if (!isAdded) return
        val newMarker =
            googleMap.addMarker(MarkerOptions().position(LatLng(event.loc_lat as Double, event.loc_lng as Double)))
        newMarker?.tag = event

        googleMap.run {
            uiSettings?.isMyLocationButtonEnabled = true


            val afterCheckPermision = {
                isMyLocationEnabled = true
            }

            PermissionsHelper.checkLocationPermissions(activity as FragmentActivity, afterCheckPermision)
            gpsHelper.isGPSEnable(activity as FragmentActivity)

            setOnMyLocationButtonClickListener {
                false
            }
        }

        val resID = getResources().getIdentifier("event_marker", "drawable", (activity as FragmentActivity).packageName)
        newMarker?.setIcon(
            (BitmapDescriptorFactory.fromBitmap(
                VectorToBitmapConverter.getBitmapFromVectorDrawable(
                    context as Context,
                    resID
                )
            ))
        )
    }
}
