package com.easy.easyriderapp.ui.main.events.add

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController

import com.easy.easyriderapp.R
import com.easy.easyriderapp.utils.PermissionsHelper
import com.easy.easyriderapp.utils.gpsHelper
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ChooseLocationOnMapFragment : Fragment(), GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnMyLocationClickListener,
   OnMapReadyCallback {
    override fun onMyLocationClick(p0: Location) {
    }


    override fun onMyLocationButtonClick(): Boolean {
        return false
    }


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {

        p0?.uiSettings?.isMyLocationButtonEnabled = true


        val afterCheckPermision : () -> Unit = {
            p0?.setMyLocationEnabled(true)
        }
        PermissionsHelper.checkLocationPermissions(activity as FragmentActivity,afterCheckPermision)
        gpsHelper.isGPSEnable(activity as FragmentActivity)


        p0?.setMyLocationEnabled(true)
        p0?.setOnMyLocationButtonClickListener(this)
        p0?.setOnMyLocationClickListener(this)


        p0?.setOnMapLongClickListener {




            val args = Bundle()
            args.putDouble("LAT", it.latitude)
            args.putDouble("LNG", it.longitude)

            findNavController().navigate(
                R.id.addEventFragment,
                args,
                NavOptions.Builder().setPopUpTo(R.id.addEventFragment, false).build()

            )

        }
    }

    private lateinit var mapView: MapView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.choose_location_on_map_fragment, container, false)

        mapView = v?.findViewById<MapView>(R.id.choose_location_map_view) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        return v
    }


}
