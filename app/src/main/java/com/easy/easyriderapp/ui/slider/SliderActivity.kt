package com.easy.easyriderapp.ui.slider

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.SliderPagerAdapter
import com.easy.easyriderapp.ui.BaseActivity
import com.easy.easyriderapp.viewmodels.slider.SliderActivityViewModel
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.google.android.material.tabs.TabLayout
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_slider.*
import javax.inject.Inject

class SliderActivity : BaseActivity<SliderActivityViewModel>() {



    private lateinit var pagerAdapter: SliderPagerAdapter
    override lateinit var viewModel: SliderActivityViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidInjection.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SliderActivityViewModel::class.java)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slider)

        val viewPagerInSliderActivity: ViewPager = findViewById(R.id.viewPagerInSliderActivity)
        pagerAdapter = SliderPagerAdapter(supportFragmentManager, slidesArray)
        viewPagerInSliderActivity.adapter = pagerAdapter

        val tabLayout = findViewById<TabLayout>(R.id.tabLayoutInSliderActivity)
        tabLayout.setupWithViewPager(viewPagerInSliderActivity, true)

        buttonSignIn.setOnClickListener {
            viewModel.navigateToLogIn()
        }
        buttonGetStarted.setOnClickListener {
            viewModel.navigateToRegister()
        }
    }

    override fun onStart() {
        viewModel.navigateToMainView()
        super.onStart()
    }

}

