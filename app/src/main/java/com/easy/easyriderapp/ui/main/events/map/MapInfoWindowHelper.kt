package com.easy.easyriderapp.ui.main.events.map

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.utils.DistancesUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MapInfoWindowHelper(
    val context: Context,
    val buttonInfo: View,
    val mapWrapperLayout: MapWrapperLayout,
    val navigateToEventView : (event :Event ) -> Unit

) {

    fun initMapWrapperLayout(googleMap:GoogleMap) {
        mapWrapperLayout.init(googleMap, DistancesUtils.getPixelsFromDp(context, (39 + 20).toFloat()))
    }


    val infoButtonListener = object : OnInfoWindowElemTouchListener(
        buttonInfo,
        context.resources.getDrawable(R.drawable.common_google_signin_btn_text_dark_normal),
        context.resources.getDrawable(R.drawable.common_google_signin_btn_icon_light_focused)
    ) {

        override fun onClickConfirmed(v: View, marker: Marker) {
            val markerEvent = marker.tag as Event

            navigateToEventView(Event(
                eventName = markerEvent.eventName.toString(),
                description = markerEvent.description,
                category = markerEvent.category.toString(),
                loc_lat = markerEvent.loc_lat,
                loc_lng = markerEvent.loc_lng,
                day = markerEvent.day,
                month = markerEvent.month,
                year = markerEvent.year,
                creatorName = markerEvent.creatorName
            ))
        }
    }

    fun setInfoButtonListener() {
        buttonInfo.setOnTouchListener(infoButtonListener)
    }

    fun setOnMarkerClickListener(googleMap:GoogleMap) {
        googleMap.setOnMarkerClickListener {
            it?.showInfoWindow()
            false
        }
    }

    fun getInfoWindow(marker: Marker, infoWindow:ViewGroup) :View{

        infoButtonListener.setMarker(marker)

        mapWrapperLayout?.setMarkerWithInfoWindow(marker, infoWindow)

        val markerEvent = marker?.tag as Event
        val category: String = markerEvent.category as String
        val eventName: String = markerEvent.eventName as String
        val distanceFromUser = markerEvent.distanceFromUser.toString()
        val eventDateString = "${markerEvent.day.toString()}/${markerEvent.month.toString()}/${markerEvent.year.toString()}"

        infoWindow.findViewById<TextView>(R.id.eventMapInfoDistanceTextView).setText("Distance to event: $distanceFromUser km")
        infoWindow.findViewById<TextView>(R.id.eventMapInfoDateTextView).setText(eventDateString)
        infoWindow.findViewById<TextView>(R.id.eventMapInfoNameTextView).setText(eventName)
        infoWindow.findViewById<TextView>(R.id.eventMapInfoCategoryTextView).setText(category)

        return infoWindow
    }


}