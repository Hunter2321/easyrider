package com.easy.easyriderapp.ui.main.events.add

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController

import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.models.StringWithTag
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.events.AddEventFragmentViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.add_event_fragment.*
import java.util.*
import javax.inject.Inject


class AddEventFragment : Fragment(), OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: AddEventFragmentViewModel
    var mapView: MapView? = null

    var lat: Double? = null
    var lng: Double? = null
    val eventDateInCalendar = Calendar.getInstance()


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        lat = arguments?.getDouble("LAT")
        lng = arguments?.getDouble("LNG")


        super.onAttach(context)
    }

    override fun onMapReady(p0: GoogleMap?) {
        p0 ?: return
        if (lat != null && lng != null) {
            mapView?.visibility = VISIBLE
            p0.clear()
            val mo = MarkerOptions().position(LatLng(lat as Double, lng as Double))
            p0.addMarker(mo)
            p0.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat as Double, lng as Double), 13.0f))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.add_event_fragment, container, false)
        mapView = v?.findViewById(R.id.eventLocationMap)
        mapView?.onCreate(savedInstanceState)
        mapView?.onResume()
        mapView?.getMapAsync(this)

        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AddEventFragmentViewModel::class.java)
        bindObservables()

        context?.let {

            val eventDataValidation = EventDataValidation(context as Context)

            eventDataValidation.addValidationListenerForEventName(
                textInputLayoutEventNameInAddEvent,
                eventNameEditTextInAddEventView,
                viewModel.eventNameLiveData
            )

            eventDataValidation.addDateDialogListener(
                eventDateTextInputLayoutInAddEventFragment,
                eventDateEditTextInAddEvent,
                viewModel.eventDateStringLiveData,
                viewModel.eventDateLongLiveData
            )

            eventDataValidation.addDescriptionChangedListener(
                eventDescriptionEditTextInAddEventView,
                viewModel.eventDescriptionLiveData
            )

            eventDataValidation.addCategoryChangedListener(chooseCategorySpinner, viewModel.eventCategoryIdLiveData)

            chooseLocation.setOnClickListener {
                findNavController().navigate(
                    R.id.choose_location,
                    null,
                    NavOptions.Builder().setPopUpTo(R.id.addEventFragment, true).build()
                )
            }

            createEventButton.setOnClickListener {
                if (!eventDataValidation.isEventNameValid(eventNameEditTextInAddEventView.text.toString())) {
                    return@setOnClickListener
                } else if (TextUtils.isEmpty(eventDateEditTextInAddEvent.text)) {
                    Toast.makeText(context, "You have to choose a dateInMillis...", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                } else if (lat == null || lng == null) {
                    Toast.makeText(context, "You have to choose a location...", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                } else {
                    viewModel.createNewEvent(
                        Event(
                            eventName = eventNameEditTextInAddEventView.text.toString(),
                            dateInMillis = eventDateInCalendar.timeInMillis,
                            description = eventDescriptionEditTextInAddEventView.text.toString(),
                            category = (chooseCategorySpinner.selectedItem as StringWithTag).stringPart,
                            categoryTag = (chooseCategorySpinner.selectedItem as StringWithTag).tagPart.toString(),
                            loc_lat = lat as Double,
                            loc_lng = lng as Double,
                            year = eventDateInCalendar.get(Calendar.YEAR),
                            month = eventDateInCalendar.get(Calendar.MONTH) + 1,
                            day = eventDateInCalendar.get(Calendar.DAY_OF_MONTH)
                        )
                    )
                    clearViewsAfterCreateEvent()
                    Toast.makeText(context,"Event has been created!", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.eventsMap)
                }

            }
        }
    }


    fun clearViewsAfterCreateEvent(){
        viewModel.eventNameLiveData.value = ""
        viewModel.eventDateStringLiveData.value = ""
        viewModel.eventDescriptionLiveData.value =""
        viewModel.eventDateLongLiveData.value = 0
        lat = null
        lng = null
    }

    fun bindObservables() {

        viewModel.eventNameLiveData.observe(this, androidx.lifecycle.Observer {
            eventNameEditTextInAddEventView.setText(it)
        })

        viewModel.eventDateStringLiveData.observe(this, androidx.lifecycle.Observer {
            eventDateEditTextInAddEvent.setText(it)
        })
        viewModel.eventDateLongLiveData.observe(this,androidx.lifecycle.Observer {
            eventDateInCalendar.timeInMillis = it
        })

        viewModel.eventDescriptionLiveData.observe(
            this,
            androidx.lifecycle.Observer { eventDescriptionEditTextInAddEventView.setText(it) })

        viewModel.eventCategoryIdLiveData.observe(this, androidx.lifecycle.Observer {
            chooseCategorySpinner.setSelection(it)
        })


    }


    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapView?.onLowMemory()
        super.onLowMemory()
    }

    override fun onPause() {
        mapView?.onPause()

        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

}
