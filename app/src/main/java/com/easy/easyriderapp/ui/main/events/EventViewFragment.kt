package com.easy.easyriderapp.ui.main.events

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.easy.easyriderapp.R
import com.easy.easyriderapp.viewmodels.main.events.EventViewFragmentViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.event_view_fragment.*

class EventViewFragment : Fragment(), OnMapReadyCallback {

     var mapView: MapView? = null
     var googleMap: GoogleMap?= null

    override fun onMapReady(p0: GoogleMap?) {
        p0 ?: return

        googleMap = p0
        val lat = (arguments as Bundle).getDouble("LAT")
        val lng = (arguments as Bundle).getDouble("LNG")


        mapView?.visibility = View.VISIBLE
        p0.clear()
        val mo = MarkerOptions().position(LatLng(lat, lng))
        p0.addMarker(mo)
        p0.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 13.0f))

    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    private lateinit var viewModel: EventViewFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.event_view_fragment, container, false)

        mapView = v?.findViewById(R.id.eventMapInEventView)
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)

        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(EventViewFragmentViewModel::class.java)

        if (arguments != null && arguments is Bundle) {

            eventNameTextView.setText("${(arguments as Bundle).getString("EVENT_NAME")}")
            eventCategoryTextView.setText("Category: ${(arguments as Bundle).getString("CATEGORY")}")
            adminNameTextView.setText("Admin: ${(arguments as Bundle).getString("ADMIN_NAME")}")
            eventDateTextView.setText("Date: ${(arguments as Bundle).getString("DATE_STRING")}")
            eventDescriptionTextView.setText("Description: ${(arguments as Bundle).getString("DESCRIPTION")}")

        }
    }

    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        mapView?.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        mapView?.onLowMemory()
        super.onLowMemory()
    }

    override fun onPause() {
        mapView?.onPause()

        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

}
