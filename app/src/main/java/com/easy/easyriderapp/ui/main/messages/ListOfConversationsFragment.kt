package com.easy.easyriderapp.ui.main.messages

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.RecyclerViewAdapterForConvesationsList
import com.easy.easyriderapp.models.Conversation
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.services.ProgressDialogService
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.messages.ListOfConversationsFragmentViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.list_of_conversations_fragment.*
import javax.inject.Inject

class ListOfConversationsFragment : Fragment() {

    private lateinit var viewModel: ListOfConversationsFragmentViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val dataLoadingProgressDialog = ProgressDialogService()


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dataLoadingProgressDialog.run {
            create(context as Context)
            setMessage("Data loading...")
            showDialog()
        }

        return inflater.inflate(R.layout.list_of_conversations_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ListOfConversationsFragmentViewModel::class.java)

        createNewConversationFloatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.createNewConversationFragment)
        }

        val navigateToChat = { user: User ->
            val args = Bundle()
            args.putString("RECEIVER_ID", user.userId)
            args.putString("RECEIVER_NICKNAME", user.nickname)
            findNavController().navigate(R.id.chatFragment, args)
        }

        val dummyData = mutableListOf<Conversation>()
        val recyclerViewAdapter = RecyclerViewAdapterForConvesationsList(context as Context, dummyData, navigateToChat)
        conversationsRecyclerViewInListOfConversations.run{
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            adapter = recyclerViewAdapter
        }

        viewModel.conversations.observe(this, Observer {
            dataLoadingProgressDialog.hideDialog()

            recyclerViewAdapter.updateData(it)

        })
    }
}
