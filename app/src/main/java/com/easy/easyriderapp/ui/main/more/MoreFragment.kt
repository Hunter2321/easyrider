package com.easy.easyriderapp.ui.main.more

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.adapters.MoreFragmentRecyclerViewAdapter
import com.easy.easyriderapp.models.MenuItem
import com.easy.easyriderapp.ui.slider.SliderActivity
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import com.easy.easyriderapp.viewmodels.main.more.MoreFragmentViewModel
import com.google.firebase.auth.FirebaseAuth
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.more_fragment.*
import javax.inject.Inject


class MoreFragment : Fragment() {
    private lateinit var viewModel: MoreFragmentViewModel
    lateinit var listAdapter: MoreFragmentRecyclerViewAdapter

    @Inject
    lateinit var viewFactory: ViewModelFactory

    var listOfItems = mutableListOf<MenuItem>()

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v = inflater.inflate(R.layout.more_fragment, container, false)

        listOfItems.clear()
//        listOfItems.add(MenuItem("Profile", R.drawable.person_icon,1))
//        listOfItems.add(MenuItem("Users", R.drawable.people_icon,2))
        listOfItems.add(MenuItem("Search Settings", R.drawable.search_settings_icon,3))

        val onClickMethod ={id:Int ->
            when(id){
//                1->{findNavController().navigate(R.id.profileFragment)}
//                2->{findNavController().navigate(R.id.listOfUserFragment)}
                3->{findNavController().navigate(R.id.searchSettingsFragment)}
            }
        }

        val moreRecyclerView = v?.findViewById<RecyclerView>(R.id.moreRecyclerView)
        listAdapter = MoreFragmentRecyclerViewAdapter(context as Context, listOfItems, onClickMethod)
        moreRecyclerView?.setHasFixedSize(true)

        moreRecyclerView?.layoutManager = LinearLayoutManager(context)
        moreRecyclerView?.adapter = listAdapter



        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewFactory).get(MoreFragmentViewModel::class.java)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        logout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            val intent = Intent(context, SliderActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

    }
}


