package com.easy.easyriderapp.ui.main.events.map

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MapWrapperLayout : LinearLayout {
    /**
     * Reference to a GoogleMap object
     */
    lateinit var map: GoogleMap
    /**
     * Vertical offset in pixels between the bottom edge of our InfoWindow
     * and the marker position (by default it's bottom edge too).
     * It's a good idea to use custom markers and also the InfoWindow frame,
     * because we probably can't rely on the sizes of the default marker and frame.
     */
    private var bottomOffsetPixels: Int = 0
    /**
     * A currently selected marker
     */
    var markerOfEvent: Marker? = null
    /**
     * Our custom view which is returned from either the InfoWindowAdapter.getInfoContents
     * or InfoWindowAdapter.getInfoWindow
     */
    var infoWindowOfEvent: View? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    /**
     * Must be called before we can route the touch events
     */
    fun init(map: GoogleMap, bottomOffsetPixels: Int) {
        this.map = map
        this.bottomOffsetPixels = bottomOffsetPixels
    }

    /**
     * Best to be called from either the InfoWindowAdapter.getInfoContents
     * or InfoWindowAdapter.getInfoWindow.
     */
    fun setMarkerWithInfoWindow(marker: Marker, infoWindow: View) {
        markerOfEvent = marker
        infoWindowOfEvent = infoWindow
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {

        var ret = false
        // Make sure that the infoWindow is shown and we have all the needed infoWindowOfEvent
        if (markerOfEvent != null && markerOfEvent?.isInfoWindowShown() as Boolean && map != null && infoWindowOfEvent != null) {
            // Get a marker position on the screen
            val marker = markerOfEvent as Marker
            val infoWindow = infoWindowOfEvent as View
            val point = map.getProjection().toScreenLocation(marker.getPosition())
            // Make a copy of the MotionEvent and adjust it's location
            // so it is relative to the infoWindow left top corner
            val copyEv = MotionEvent.obtain(ev)
            val deltaX = (-point.x + (infoWindow.getWidth() / 2)).toFloat()
            val deltaY = (-point.y + infoWindow.getHeight() + bottomOffsetPixels).toFloat()
            copyEv.offsetLocation(deltaX,deltaY)
            // Dispatch the adjusted MotionEvent to the infoWindow
            ret = infoWindow.dispatchTouchEvent(copyEv)
        }
        // If the infoWindow consumed the touch event, then just return true.
        // Otherwise pass this event to the super class and return it's result
        return ret || super.dispatchTouchEvent(ev)
    }
}