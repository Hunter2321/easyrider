package com.easy.easyriderapp.repositories

import android.location.Location
import io.reactivex.Observable

interface AddressRepository {
    fun getAddress(location: Location) : Observable<String>

}