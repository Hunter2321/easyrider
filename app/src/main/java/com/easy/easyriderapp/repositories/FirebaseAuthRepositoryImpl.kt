package com.easy.easyriderapp.repositories

import android.util.Log
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.models.UserAuthInfo
import com.easy.easyriderapp.utils.SingleLiveEvent
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class FirebaseAuthRepositoryImpl : FirebaseAuthRepository {


    val newUserCreated = SingleLiveEvent<Task<AuthResult>>()
    val logInRequest = SingleLiveEvent<Task<AuthResult>>()

    private val firebaseAuth = FirebaseAuth.getInstance()

    private val firebaseDatabaseReference = FirebaseDatabase.getInstance()
    private var firebaseDatabaseCurrentUserReference: DatabaseReference? = null

    override fun getCurrentLoggedUserId(): String? {
        return firebaseAuth.currentUser?.uid
    }


    override fun logInWithEmailAndPassword(userInfo: UserAuthInfo): SingleLiveEvent<Task<AuthResult>> {

        firebaseAuth.signInWithEmailAndPassword(userInfo.email, userInfo.password)
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.w("FirebaseLog", "logInWithEmail:success")

                } else {
                    Log.w("FirebaseLog", "logInWithEmail:failure", task.exception)
                }
                logInRequest.value = task
            })

        return logInRequest
    }

    override fun createNewUser(userInfo: UserAuthInfo): SingleLiveEvent<Task<AuthResult>> {
        val nickname = userInfo.firstName + " " + userInfo.lastName

        firebaseAuth.createUserWithEmailAndPassword(userInfo.email, userInfo.password)
            .addOnCompleteListener { task ->

                if (task.isSuccessful) {
                    Log.d("FirebaseLOG", "createUserWithEmail:success")

                    firebaseDatabaseCurrentUserReference =
                        firebaseDatabaseReference.getReference("users").child(getCurrentLoggedUserId() as String)


                    (firebaseDatabaseCurrentUserReference as DatabaseReference)
                        .setValue(
                            User(
                                firstName = userInfo.firstName as String,
                                lastName = userInfo.lastName as String,
                                nickname = nickname,
                                email = userInfo.email,
                                userId = getCurrentLoggedUserId().toString(),
                                dateFrom = 1551744000000,
                                dateTo = 1635717620666
                            )
                        )
                } else {
                    Log.w("FirebaseLog", "createUserWithEmail:failure", task.exception)
                }

                newUserCreated.value = task
            }

        return newUserCreated
    }


    override fun getCreatedUserAuthResult(): SingleLiveEvent<Task<AuthResult>> {
        return newUserCreated
    }

    override fun getLogInResult(): SingleLiveEvent<Task<AuthResult>> {
        return logInRequest
    }
}
