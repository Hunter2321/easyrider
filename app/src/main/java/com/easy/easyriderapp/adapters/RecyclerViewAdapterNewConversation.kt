package com.easy.easyriderapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.User



class RecyclerViewAdapterNewConversation(var context: Context, var data: MutableList<User>, val navigateToChat : (user:User) -> Unit) : RecyclerView.Adapter<RecyclerViewAdapterNewConversation.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        val v = LayoutInflater.from(context).inflate(R.layout.contact_item, parent, false)

        val vHolder = MyViewHolder(v)

        vHolder.contactItem.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val userId = vHolder.receiverId
                val userName = vHolder.receiverNickname

                navigateToChat(User(userId = userId, nickname = userName))
            }

        })
        return vHolder
    }

    override fun getItemCount(): Int {

        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.receiverNickname = data.get(position).nickname
        holder.receiverId = data.get(position).userId

        holder.firstNameAndLastNameTextView?.setText(holder.receiverNickname)

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var receiverId :String? = null
        var receiverNickname:String? = null

        val contactItem = itemView.findViewById<ConstraintLayout>(R.id.contact_item)
        val firstNameAndLastNameTextView = itemView.findViewById<TextView>(R.id.firstNameAndLastNameTextView)
    }
    fun updateData (newList: MutableList<User>){

        data = newList
        notifyDataSetChanged()
    }

}