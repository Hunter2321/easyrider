package com.easy.easyriderapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.MenuItem

class MoreFragmentRecyclerViewAdapter(var context: Context, var data: MutableList<MenuItem>, val onClick : (Int) -> Unit) : RecyclerView.Adapter<MoreFragmentRecyclerViewAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.menu_list_item, parent, false)
        val vHolder = MyViewHolder(v)
        vHolder.menuListItem.setOnClickListener {
            onClick(vHolder.itemId)
        }
        return vHolder
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.menuTitleTextView.setText(data.get(position).title)
        holder.menuIconImageView.setImageResource(data.get(position).icon)
        holder.itemId = data.get(position).id
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val menuListItem: ConstraintLayout = itemView.findViewById(R.id.menu_list_item)
        val menuTitleTextView: TextView = itemView.findViewById(R.id.categoryInStartSettingsTextView)
        val menuIconImageView: ImageView = itemView.findViewById(R.id.menuIconImageView)
        var itemId : Int = 0

    }

    override fun getItemCount(): Int {
        return data.size
    }
}