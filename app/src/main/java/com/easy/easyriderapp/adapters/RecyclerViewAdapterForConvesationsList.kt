package com.easy.easyriderapp.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Conversation
import com.easy.easyriderapp.models.User


class RecyclerViewAdapterForConvesationsList(var context: Context, var data: List<Conversation>, val navigateToChat : (user: User) -> Unit) :
    RecyclerView.Adapter<RecyclerViewAdapterForConvesationsList.MyViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(context).inflate(R.layout.conversation_list_item, parent, false)
        val vHolder = MyViewHolder(v)

        vHolder.conversationItem.setOnClickListener{
            val userId = vHolder.receiverId
            val userName = vHolder.receiverName

            navigateToChat(User(userId = userId, nickname = userName))
        }



        return vHolder
    }



    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.receiverNameTextView.setText(data.get(position).receiverName)
        holder.lastMessageTextView.setText(data.get(position).lastMessage)



        holder.receiverId = data.get(position).receiverId as String
        holder.receiverName = data.get(position).receiverName as String
        holder.isRead = data.get(position).isRead





        if (holder.isRead) {

            holder.receiverNameTextView.setTypeface(holder.receiverNameTextView.typeface, Typeface.NORMAL)
            holder.lastMessageTextView.setTypeface(holder.lastMessageTextView.typeface, Typeface.NORMAL)

            holder.receiverNameTextView.setTextColor(Color.GRAY)
            holder.lastMessageTextView.setTextColor(Color.GRAY)

        } else {

            holder.receiverNameTextView.setTypeface(holder.receiverNameTextView.typeface, Typeface.BOLD)
            holder.lastMessageTextView.setTypeface(holder.lastMessageTextView.typeface, Typeface.BOLD)

            holder.receiverNameTextView.setTextColor(Color.BLACK)
            holder.lastMessageTextView.setTextColor(Color.BLACK)

        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val conversationItem = itemView.findViewById<ConstraintLayout>(R.id.conversationItem)

        val receiverNameTextView: TextView = itemView.findViewById(R.id.userName)
        val lastMessageTextView: TextView = itemView.findViewById(R.id.lastMessage)

        lateinit var receiverName : String
        lateinit var receiverId: String
        var isRead: Boolean = false

    }
    override fun getItemCount(): Int {
        return data.size
    }

    fun updateData (newList: MutableList<Conversation>){

        data = newList
        notifyDataSetChanged()
    }
}