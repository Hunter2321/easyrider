package com.easy.easyriderapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.easy.easyriderapp.R
import com.easy.easyriderapp.models.Message

class RecyclerViewAdapterForChatMessages(
    var context: Context,
    var data: MutableList<Message>,
    var currentUserId: String
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        if (data.get(position).fromId == currentUserId) {
            return 1
        } else {
            return 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        lateinit var vHolder: RecyclerView.ViewHolder
        if (viewType == 0) {
            val v = LayoutInflater.from(context).inflate(R.layout.layout_of_messages_from_another_user, parent, false)
            vHolder = VieHolderForMessagesFromAnotherUser(v)

        } else if (viewType == 1) {
            val v = LayoutInflater.from(context).inflate(R.layout.layout_of_messages_from_current_user, parent, false)
            vHolder = ViewHolderForMessagesFromCurrentUser(v)
        }
        return vHolder
    }

    override fun getItemCount(): Int {


        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder?.itemViewType) {
            1 -> {
                val vieHolderForMessagesFromCurrentUser = holder as ViewHolderForMessagesFromCurrentUser
                vieHolderForMessagesFromCurrentUser.messageText.setText(data.get(position).text)
            }

            0 -> {
                val vieHolderForMessagesFromAnotherUser = holder as VieHolderForMessagesFromAnotherUser
                vieHolderForMessagesFromAnotherUser.messageText.setText(data.get(position).text)

            }
        }

    }


    class ViewHolderForMessagesFromCurrentUser(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val messageText: TextView = itemView.findViewById(R.id.messageText)

    }

    class VieHolderForMessagesFromAnotherUser(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val messageText: TextView = itemView.findViewById(R.id.messageText)

    }


    fun updateData(messages: MutableList<Message>) {
        data = messages
        notifyDataSetChanged()
    }


}