package com.easy.easyriderapp.utils

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener

class FirebaseValueEventLiveData(val reference : DatabaseReference, val singleValueEvent:Boolean) : LiveData<DataSnapshot>() {
    private val LOG_TAG = "FirebaseQueryLiveData"

    val listener = object : ValueEventListener{
        override fun onCancelled(p0: DatabaseError) {
        }
        override fun onDataChange(p0: DataSnapshot) {
            value = p0
        }
    }

    override fun onActive() {
        super.onActive()
        Log.d(LOG_TAG, "onInactive");
        if(singleValueEvent) reference.addListenerForSingleValueEvent(listener) else reference.addValueEventListener(listener)
    }

    override fun onInactive() {
        Log.d(LOG_TAG, "onInactive");
        reference.removeEventListener(listener)

    }
}