package com.easy.easyriderapp.utils

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import android.widget.Toast

object gpsHelper {

    fun isGPSEnable(activity: Activity) {
        val manager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            Toast.makeText(activity,"Turn on GPS navigation to search your current position...", Toast.LENGTH_SHORT).show()
        }
    }
}