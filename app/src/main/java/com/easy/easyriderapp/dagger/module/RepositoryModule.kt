package com.easy.easyriderapp.dagger.module

import android.app.Application
import android.content.Context
import com.easy.easyriderapp.repositories.*
import com.google.android.gms.location.FusedLocationProviderClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideFirebaseAuthRepository () = FirebaseAuthRepositoryImpl() as FirebaseAuthRepository

    @Singleton
    @Provides
    fun provideFirebaseDatabaseRepository (provideFirebaseAuthRepository:FirebaseAuthRepository) = FirebaseDatabaseRepositoryImpl(provideFirebaseAuthRepository) as FirebaseDatabaseRepository

    @Singleton
    @Provides
    fun provideLocationRepository (context : Context, fusedLocationProviderClient: FusedLocationProviderClient, addressRepository: AddressRepository)
            = LocationRepositoryImpl(context as Application, fusedLocationProviderClient,addressRepository) as LocationRepository


    @Singleton
    @Provides
    fun provideAddressRepository (context : Context) = AddressRepositoryImpl(context) as AddressRepository

    @Provides
    fun provideFusedLocationProviderClient (context: Context) = FusedLocationProviderClient(context)

}