package com.easy.easyriderapp.dagger.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.easy.easyriderapp.viewmodels.MainActivityViewModel
import com.easy.easyriderapp.viewmodels.slider.SliderActivityViewModel
import com.easy.easyriderapp.viewmodels.ViewModelFactory
import dagger.Binds
import dagger.MapKey
import dagger.Module
import kotlin.reflect.KClass
import com.easy.easyriderapp.viewmodels.auth.LoginActivityViewModel
import com.easy.easyriderapp.viewmodels.auth.RegisterActivityViewModel
import com.easy.easyriderapp.viewmodels.main.events.AddEventFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.events.EventViewFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.events.EventsListFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.events.EventsMapFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.messages.ChatFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.messages.CreateNewConversationFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.messages.ListOfConversationsFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.more.MoreFragmentViewModel
import com.easy.easyriderapp.viewmodels.main.more.SearchSettingsFragmentViewmodel
import dagger.multibindings.IntoMap
import javax.inject.Singleton


@Module
abstract class ViewModelModule {

//    @Provides
//    fun viewModelFactory(providerMap: Provider<SliderActivityViewModel>): ViewModelFactory {
//        return ViewModelFactory(providerMap)
//    }



    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory



    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
    @MapKey
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)


    @Binds
    @IntoMap
    @ViewModelKey(SliderActivityViewModel::class)
    internal abstract fun bindSliderViewModel(userViewModel: SliderActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterActivityViewModel::class)
    internal abstract fun bindRegisterViewModel(userViewModel: RegisterActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginActivityViewModel::class)
    internal abstract fun bindLoginViewModel(userViewModel: LoginActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun bindMainActivityViewModel(userViewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventsListFragmentViewModel::class)
    internal abstract fun bindEventsListFragmentViewModel(userViewModel: EventsListFragmentViewModel): ViewModel

    @Singleton
    @Binds
    @IntoMap
    @ViewModelKey(AddEventFragmentViewModel::class)
    internal abstract fun bindAddEventViewModel(userViewModel: AddEventFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MoreFragmentViewModel::class)
    internal abstract fun bindMoreFragmentViewModel(userViewModel: MoreFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchSettingsFragmentViewmodel::class)
    internal abstract fun bindSearchSettingsFragmentViewModel(userViewModel: SearchSettingsFragmentViewmodel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventViewFragmentViewModel::class)
    internal abstract fun bindEventViewFragmentViewModel(userViewModel: EventViewFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventsMapFragmentViewModel::class)
    internal abstract fun bindEventMapFragmentViewModel(userViewModel: EventsMapFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListOfConversationsFragmentViewModel::class)
    internal abstract fun bindListOfConversationsFragmentViewModel(userViewModel: ListOfConversationsFragmentViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(CreateNewConversationFragmentViewModel::class)
    internal abstract fun bindCreateNewConversationFragmentViewModel(userViewModel: CreateNewConversationFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChatFragmentViewModel::class)
    internal abstract fun bindChatFragmentViewModelFragmentViewModel(userViewModel: ChatFragmentViewModel): ViewModel


}

