package com.easy.easyriderapp.viewmodels.auth

import android.app.Application
import android.content.Intent
import androidx.lifecycle.Transformations
import com.easy.easyriderapp.models.UserAuthInfo
import com.easy.easyriderapp.repositories.FirebaseAuthRepository
import com.easy.easyriderapp.ui.MainActivity
import com.easy.easyriderapp.utils.SingleLiveEvent
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class LoginActivityViewModel @Inject constructor( application: Application, val firebaseAuthRepository: FirebaseAuthRepository): BaseViewModel(application){

    val loginInfo = SingleLiveEvent<UserAuthInfo>()

    val isLogInMadeSuccesfully = Transformations.switchMap(loginInfo,{
        loginInfo -> firebaseAuthRepository.logInWithEmailAndPassword(loginInfo)
    })



    fun navigateToApp(){
        val intent = Intent(context(), MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity.value = intent
        closeActivity.call()
    }

}
