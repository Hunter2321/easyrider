package com.easy.easyriderapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory
@Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?:
        creators.asIterable().firstOrNull { modelClass.isAssignableFrom(it.key) }?.value
        ?: throw IllegalArgumentException("unknown model class " + modelClass)

        return try {
            creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}

//class ViewModelFactory @Inject constructor(val myViewModelProvider : Provider<SliderActivityViewModel>) : ViewModelProvider.Factory {
//
//    @NonNull
//    @SuppressWarnings("unchecked")
//    override fun <T : ViewModel> create(@NonNull modelClass:Class<T>):T {
//        val viewModel: ViewModel
//
//
//        if (modelClass == SliderActivityViewModel::class.java)
//        {
//            viewModel = myViewModelProvider.get()
//        }
//        else
//        {
//            throw RuntimeException("unsupported view model class: " + modelClass)
//        }
//        return viewModel as T
//    }
//}

