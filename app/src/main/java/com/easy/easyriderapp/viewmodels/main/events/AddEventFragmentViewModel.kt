package com.easy.easyriderapp.viewmodels.main.events

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.easy.easyriderapp.models.Event
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.repositories.FirebaseAuthRepository
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class AddEventFragmentViewModel @Inject constructor (application : Application, val firebaseDatabase : FirebaseDatabaseRepository, val firebaseAuthRepository: FirebaseAuthRepository) : BaseViewModel(application) {

    val eventNameLiveData = MutableLiveData<String>()
    val eventDateStringLiveData = MutableLiveData<String>()
    val eventDateLongLiveData = MutableLiveData<Long>()
    val eventDescriptionLiveData = MutableLiveData<String>()
    val eventCategoryIdLiveData = MutableLiveData<Int>()


     fun createNewEvent(event: Event){
         var eventToMakeUp = event
         eventToMakeUp.creatorId = firebaseAuthRepository.getCurrentLoggedUserId()

         val getCurrentUserInfo =firebaseDatabase.getCurrentUserInfo()
         getCurrentUserInfo.observeForever(object : Observer<User>{
             override fun onChanged(t: User?) {
                 eventToMakeUp.creatorName = t?.nickname

                 firebaseDatabase.createNewEvent(eventToMakeUp)
                 getCurrentUserInfo.removeObserver(this)
             }
         })
     }
}