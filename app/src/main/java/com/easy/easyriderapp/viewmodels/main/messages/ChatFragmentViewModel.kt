package com.easy.easyriderapp.viewmodels.main.messages

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.easy.easyriderapp.models.Message
import com.easy.easyriderapp.models.User
import com.easy.easyriderapp.repositories.FirebaseAuthRepository
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class ChatFragmentViewModel @Inject constructor(
    application: Application,
    val firebaseDatabaseRepository: FirebaseDatabaseRepository,
    val firebaseAuthRepository: FirebaseAuthRepository
) : BaseViewModel(application) {

    var currentUserName:String? = null

    init {
        firebaseDatabaseRepository.getCurrentUserInfo().observeForever(object : Observer<User>{
            override fun onChanged(t: User?) {
                currentUserName = t?.nickname
                firebaseDatabaseRepository.getCurrentUserInfo().removeObserver(this)
            }
        })
    }

    fun getCurrentUserId () = firebaseAuthRepository.getCurrentLoggedUserId()


    fun getTimeStamp() = System.currentTimeMillis()

    fun sendMessageToDatabase(message: Message) {
        val messageToMakeUp: Message? = message

        messageToMakeUp?.timestamp = getTimeStamp()
        messageToMakeUp?.fromId = firebaseAuthRepository.getCurrentLoggedUserId()
        firebaseDatabaseRepository.saveMessageToDatabase(messageToMakeUp as Message)

    }

    fun fetchMessages(receiverId: String): LiveData<MutableList<Message>>{
        return firebaseDatabaseRepository.fetchMessages(receiverId)
    }


    fun setMetadataForReceiverUser(receiverId:String){
        currentUserName?.let { firebaseDatabaseRepository.setMetadataForReceiverUser(receiverId,currentUserName as String)  }

    }
    fun setMetadataForCurrentUser(receiverId:String, receiverUserName:String){
        firebaseDatabaseRepository.setMetadataForCurrentUser(receiverId,receiverUserName)
    }

}
