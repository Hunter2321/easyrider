package com.easy.easyriderapp.viewmodels.main.events

import android.app.Application
import androidx.lifecycle.Transformations
import com.easy.easyriderapp.repositories.FirebaseDatabaseRepository
import com.easy.easyriderapp.viewmodels.BaseViewModel
import javax.inject.Inject

class EventsMapFragmentViewModel @Inject constructor (application : Application, val firebaseDatabaseRepository : FirebaseDatabaseRepository) : BaseViewModel(application) {

    val getUserSearchInfo = firebaseDatabaseRepository.getCurrentUserInfo()

    val getListOfEvents = Transformations.switchMap(getUserSearchInfo, {
        firebaseDatabaseRepository.getSelectedEvents(it.fatbike,it.ebike,it.enduro,it.pumptrackbmx,it.dh,it.roadbike,it.gravelcx,it.citybike,it.xc,it.dateFrom,it.dateTo,it.loc_lat,it.loc_lng,it.range )
    })
}
